FROM openjdk:9-slim

LABEL maintainer="Ashutosh ashutosh.kumar@gmail.com"

COPY target/*.jar app.jar

EXPOSE 8080

CMD [ "java", "-jar", "app.jar" ]